import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { AlertController } from 'ionic-angular';

/**
 * Generated class for the CalculatorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-calculator',
  templateUrl: 'calculator.html',
})
export class CalculatorPage {

  testCheckboxOpen: boolean;
  testCheckboxResult;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
  }

  goTo(page){
    this.navCtrl.push(HomePage);
  }

  back(){
  if(this.navCtrl.length() >=2){
    this.navCtrl.pop();
  }

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad CalculatorPage');
  }

  doCheckbox() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Current Exchange Rate');

    alert.addInput({
      type: 'checkbox',
      label: 'USD / Rtgs',
      value: 'value1',
      checked: true
    });

    alert.addInput({
      type: 'checkbox',
      label: 'USD / Bond',
      value: 'value2'
    });

    alert.addInput({
      type: 'checkbox',
      label: 'Bond / Rtgs',
      value: 'value3'
    });

    alert.addInput({
      type: 'checkbox',
      label: 'OMIR / Rtgs',
      value: 'value4'
    });

    /*alert.addInput({
      type: 'checkbox',
      label: 'Hoth',
      value: 'value5'
    });

    alert.addInput({
      type: 'checkbox',
      label: 'Jakku',
      value: 'value6'
    });

    alert.addInput({
      type: 'checkbox',
      label: 'Naboo',
      value: 'value6'
    });

    alert.addInput({
      type: 'checkbox',
      label: 'Takodana',
      value: 'value6'
    });

    alert.addInput({
      type: 'checkbox',
      label: 'Tatooine',
      value: 'value6'
    });
    */
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Okay',
      handler: data => {
        console.log('Checkbox data:', data);
        this.testCheckboxOpen = false;
        this.testCheckboxResult = data;
      }
    });
    alert.present().then(() => {
      this.testCheckboxOpen = true;
    });
}
  

}
