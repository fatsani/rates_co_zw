import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';

/**
 * Generated class for the FaqsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-faqs',
  templateUrl: 'faqs.html',
})
export class FaqsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  goTo(page){
      this.navCtrl.push(HomePage);
  }
  back(){
    if(this.navCtrl.length() >=2){
      this.navCtrl.pop();
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad FaqsPage');
  }

}
