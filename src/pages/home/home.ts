import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {RestProvider} from '../../providers/rest/rest';
import {FaqsPage} from '../faqs/faqs';
import {CalculatorPage} from '../calculator/calculator';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  rates : any;
  markets : any;
  arrayName = new Array();
  ;

  constructor(public navCtrl: NavController, public restProvider: RestProvider) {
    this.getRates();
    this.getMarkets();
  }

  goTo(page){
    if(page ==='faqs'){
    this.navCtrl.push(FaqsPage);
  }
  else if (page === 'calculator'){
    this.navCtrl.push(CalculatorPage);
  }
  else if (page === 'rates'){
    this.navCtrl.push(HomePage);
  }
}

back(){
  if (this.navCtrl.length() >=2){
    this.navCtrl.pop();
  }
}
  getRates() {
    this.restProvider.getRates()
    .then(data => {
      this.rates = data;
      //console.log(this.rates);
    });
  }
  getMarkets() {
    this.restProvider.getMarkets()
    .then(data => {
      this.markets = data;
      this.arrayName.push(this.markets);
      let obj = (data);
      var i=0;
      console.log(obj[i].base_currency_id+ " / "+ obj[i].quoted_currency_id);
      var pair =this.getCurrencyName(obj[i].base_currency_id,obj[i].quoted_currency_id);
      console.log(pair);
    });
    
    
      //this.arrayName.forEach(function(x){
        //console.log(x[1]);
      //});
  }

  getCurrencyName(id1, id2){
    var currencyName1, currencyName2;
    switch(id1){
      case 1: currencyName1 = "USD"; break;
      case 2: currencyName1 = "OMIR"; break;
      case 3: currencyName1 = "RTGS"; break;
      case 4: currencyName1 = "BOND"; break;
    }
    switch(id2){
      case 1: currencyName2 = "USD"; break;
      case 2: currencyName2 = "OMIR"; break;
      case 3: currencyName2 = "RTGS"; break;
      case 4: currencyName2 = "BOND"; break;
  }
  return currencyName1+" / "+currencyName2;

}

}
